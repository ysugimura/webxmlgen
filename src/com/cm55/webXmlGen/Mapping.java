package com.cm55.webXmlGen;

abstract class Mapping {

  final String name;
  final String urlPattern;
  
  Mapping(String name, String urlPattern) {
    this.name = name;
    this.urlPattern = urlPattern;
  }
}
