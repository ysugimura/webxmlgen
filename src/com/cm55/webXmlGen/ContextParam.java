package com.cm55.webXmlGen;

/**
 * 要するにウェブアプリに設定する任意のパラメータで、サーブレット内部から
 * 取得できる。
 * <code>
 * ServletContext ctx = getServletConfig().getServletContext();
 * String value = ctx.getInitParameter("name");
 * </code>
 * しかし、サーブレットエンジンの動作を制御するのにも使われる。例えばjettyが
 * ディレクトリインデクスを表示するのをやめさせるには、以下
 * <pre>
 * org.eclipse.jetty.servlet.Default.dirAllowed=false
 * </pre>
 * @author ysugimura
 */
class ContextParam extends NameValue {

  ContextParam(String name, String value) {
    super(name, value);
  }

  @Override
  public String toString() {
    return 
      "\n<context-param>\n" + 
      "    <param-name>" + Util.escape(name) + "</param-name>\n" + 
      "    <param-value>" + Util.escape(value) + "</param-value>\n" + 
      "</context-param>\n";
  }
}
