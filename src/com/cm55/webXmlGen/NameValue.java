package com.cm55.webXmlGen;

/**
 * Name/Valueのペア
 * @author ysugimura
 */
abstract class NameValue {

  final String name;
  final String value;
  
  NameValue(String name, String value) {
    this.name = name;
    this.value = value;
  }

}
