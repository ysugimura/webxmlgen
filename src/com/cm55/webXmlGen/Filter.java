package com.cm55.webXmlGen;

/**
 * フィルターの設定
 * 
 * https://java.keicode.com/lang/servlet-filter.php
 * http://www.nina.jp/server/slackware/webapp/tomcat_filter.html
 * http://otndnld.oracle.co.jp/document/products/as10g/1013/doc_cd/web.1013/B28596-01/filters.htm
 * @author ysugimura
 */
class Filter extends Mapping {

  final Class<? extends javax.servlet.Filter> filterClass;
  
  Filter(String name, String urlPattern, 
      Class<? extends javax.servlet.Filter> filterClass) {
    super(name, urlPattern);
    this.filterClass = filterClass;
  }
  
  @Override
  public String toString() {
    return 
      "\n<!-- " + name + " -->\n" +
      "<filter>\n" + 
      "  <filter-name>" + name + "</filter-name>\n" + 
      "  <filter-class>" + filterClass.getName() + "</filter-class>\n" + 
      "</filter>\n" + 
      "<filter-mapping>\n" + 
      "  <filter-name>" + name + "</filter-name>\n" + 
      "  <url-pattern>" + urlPattern + "</url-pattern>\n" + 
      "</filter-mapping>\n";
  }

}
