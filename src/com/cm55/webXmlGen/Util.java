package com.cm55.webXmlGen;

public class Util {

  public static String escape(String t) {
    StringBuilder sb = new StringBuilder();
    for(int i = 0; i < t.length(); i++){
       char c = t.charAt(i);
       switch(c){
       case '<': sb.append("&lt;"); break;
       case '>': sb.append("&gt;"); break;
       case '\"': sb.append("&quot;"); break;
       case '&': sb.append("&amp;"); break;
       case '\'': sb.append("&apos;"); break;
       default:
          if(c>0x7e) {
             sb.append("&#"+((int)c)+";");
          }else
             sb.append(c);
       }
    }
    return sb.toString();
 }
}
