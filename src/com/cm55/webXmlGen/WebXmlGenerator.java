package com.cm55.webXmlGen;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;

public class WebXmlGenerator {

  /** web.xmlのヘッダ決まり文句 */
  private static final String HEADER = 
      "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
      "<web-app xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" + 
      "    xmlns=\"http://java.sun.com/xml/ns/javaee\"\n" + 
      "    xsi:schemaLocation=\"http://java.sun.com/xml/ns/javaee \n" + 
      "    http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd\"\n" + 
      "    version=\"3.0\">\n";
  
  /** web.xmlのフッタ */
  private static final String FOOTER = 
      "</web-app>\n";

  Optional<String>displayName = Optional.empty();
  List<ContextParam>contextParams = new ArrayList<>();
  List<Listener>listeners = new ArrayList<>();
  List<Servlet>servlets = new ArrayList<>();
  List<Filter>filters = new ArrayList<>();
  
  public WebXmlGenerator() {
    
  }

  /**
   * 表示名を追加する 
   */
  public WebXmlGenerator setDisplayName(String name) {
    displayName = Optional.of("<display-name>" + Util.escape(name) + "</display-name>");
    return this;
  }
  
  /**
   * コンテキストパラメータを追加する

   * @param name 名称
   * @param value 値
   * @return 本オブジェクト
   */
  public WebXmlGenerator addContextParam(String name, String value) {
    addContextParam(new ContextParam(name, value));
    return this;
  }
  
  /**
   * リスナーを追加する
   * @param listenerClass リスナークラス
   * @return 本オブジェクト
   */
  public WebXmlGenerator addListener(Class<? extends EventListener> listenerClass) {
    addListener(new Listener(listenerClass));
    return this;
  }
  
  /**
   * サーブレットを追加する
   * @param servletName サーブレット名称。他と衝突しなければ何でもよい。
   * @param urlPattern このサーブレットがサービスするURLパターン
   * @param servletClass サービスを行うサーブレットのクラス
   * @return 本オブジェクト
   */
  public WebXmlGenerator addServlet(String servletName, String urlPattern, 
      Class<? extends javax.servlet.Servlet> servletClass, InitParam...initParams) {
    addServlet(new Servlet(servletName, urlPattern, servletClass, initParams));
    return this;
  }
  
  /**
   * フィルターを追加する
   * @param name フィルター名称。他と衝突しなければ何でもよい。
   * @param urlPattern URLパターン
   * @param filterClass フィルターを実現するクラス。
   * @return 本オブジェクト
   */
  public WebXmlGenerator addFilter(String name, String urlPattern, 
      Class<? extends javax.servlet.Filter> filterClass) {
    addFilter(new Filter(name, urlPattern, filterClass));
    return this;
  }
  
  WebXmlGenerator addContextParam(ContextParam param) {
    contextParams.add(param);
    return this;
  }
  
  WebXmlGenerator addListener(Listener listener) {
    listeners.add(listener);
    return this;
  }
  
  WebXmlGenerator addServlet(Servlet servlet) {
    servlets.add(servlet);
    return this;
  }
  
  WebXmlGenerator addFilter(Filter filter) {
    filters.add(filter);
    return this;
  }
  
  @Override
  public String toString() {
    return 
      HEADER + 
      displayName.map(s->s + "\n").orElse("") +
      contextParams.stream().map(c->c.toString()).collect(Collectors.joining()) +
      listeners.stream().map(l->l.toString()).collect(Collectors.joining()) +
      servlets.stream().map(s->s.toString()).collect(Collectors.joining()) +
      filters.stream().map(f->f.toString()).collect(Collectors.joining()) +
      FOOTER;
  }

  /**
   * warパスを指定してweb.xmlを出力する。
   * war/WEB-INF/web.xmlが出力される。
   * @param war warフォルダパス
   * @throws IOException 書き込みエラー
   */
  public void writeToWar(Path war) throws IOException {
    Path webInf = war.resolve("WEB-INF");
    Files.createDirectories(webInf);
    Path webXml = webInf.resolve("web.xml");
    Files.writeString(webXml,  toString());  
  }
}
