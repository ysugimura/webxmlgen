package com.cm55.webXmlGen;

public class InitParam extends NameValue {

  public InitParam(String name, String value) {
    super(name, value);
  }
  
  @Override
  public String toString() {
    return 
      "  <init-param>\n" +
      "    <param-name>" + name + "</param-name>\n" +
      "    <param-value>" + value + "</param-value>\n" +
      "  </init-param>\n";
  }
}
