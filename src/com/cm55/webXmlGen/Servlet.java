package com.cm55.webXmlGen;

import java.util.*;
import java.util.stream.*;

class Servlet extends Mapping {

  /** サーブレットクラス */
  final Class<? extends javax.servlet.Servlet> servletClass;
  
  /** パラメータ */
  final List<InitParam>initParams;
  
  Servlet(String servletName, String urlPattern, Class<? extends javax.servlet.Servlet> servletClass,
      InitParam..._initParams) {
    super(servletName, urlPattern);
    this.servletClass = servletClass;
    initParams = Arrays.stream(_initParams).collect(Collectors.toList());
  }
  
  Servlet addInitParam(String name, String value) {
    initParams.add(new InitParam(name, value));
    return this;
  }
  
  public String toString() {
    return 
      "\n<!-- " + name + "-->\n" +
      "<servlet>\n" + 
      "  <servlet-name>" + name + "</servlet-name>\n" + 
      "  <servlet-class>" + servletClass.getName() + "</servlet-class>\n" + 
      initParams.stream().map(p->p.toString()).collect(Collectors.joining()) +
      "</servlet>\n" + 
      "<servlet-mapping>\n" + 
      "  <servlet-name>" + name + "</servlet-name>\n" + 
      "  <url-pattern>" + urlPattern + "</url-pattern>\n" + 
      "</servlet-mapping>\n";
  }
}
