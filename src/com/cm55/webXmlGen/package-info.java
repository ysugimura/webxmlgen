/**
 * web.xmlジェネレータツール
 * <p>
 * サーブレットの設定はweb.xmlに記述する必要がある。これは「war/WEB-INF/web.xml」
 * というパスになる。
 * </p>
 */
package com.cm55.webXmlGen;