package com.cm55.webXmlGen;

import java.util.*;

/**
 * https://www.techscore.com/tech/Java/JavaEE/Servlet/7/
 * @author ysugimura
 */
class Listener {

  final Class<? extends EventListener> listenerClass;
  
  /** リスナー一般の登録 */
  Listener(Class<? extends EventListener> listenerClass) {
    this.listenerClass = listenerClass;
  }
  
  @Override
  public String toString() {
    return 
      "\n<listener>\n" + 
      "  <listener-class>" + listenerClass.getName() + "</listener-class>\n" + 
      "</listener>\n";
  }

}
